﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_tick_tac_toe
{

    public class player
    {
        public string huPlayer = "O";
        public string aiPlayer = "X";
        public List<List<int>> winCombo = new List<List<int>>() {
                new List<int>(){0, 1, 2},
                new List<int>(){3, 4, 5},
                new List<int>(){6, 7, 8},
                new List<int>(){0, 3, 6},
                new List<int>(){1, 4, 7},
                new List<int>(){0, 4, 8},
                new List<int>(){6, 4, 2},
                new List<int>(){2, 5, 8},

            };

    }
}
