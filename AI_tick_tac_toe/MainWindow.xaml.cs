﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AI_tick_tac_toe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Button> matrix = new List<Button>();
        player Player = new player();
        int index=0;
        bool isOver = false;

        public MainWindow()
        {           
            InitializeComponent();
            StartGame();
        }        
        private void StartGame()
        {
            isOver = false;
            matrix.Clear();
            InitializeBoard();
            AddToBoard();
        }
        private void AddToBoard()
        {
            board.Children.Clear();
           
            for (int i = 0; i < 9; i++)
            {
                board.Children.Add(matrix[i]);

            }
        }
        private void InitializeBoard()
        {
            for (int i = 0; i < 9; i++)
            {
                Button btn = new Button()
                {
                    Name = "dfdf",
                    FontSize = 120,
                    Width = 140,
                    Height = 140,
                    Content = "",
                    Tag = i,
                };
                btn.Click += Btn_Click;                
                matrix.Add(btn);
            }          
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn.Content.ToString() == "") // cho khoong click vao lan 2
            {
                int id = Int32.Parse(btn.Tag.ToString());
                turn(id, Player.huPlayer);
                if (!checkTie() && isOver == false) turn(bestSpot(), Player.aiPlayer);
            }
        }
        public int bestMove = 1;
        public int go = 1;

        private int bestSpot()
        {
            minimax(Player.aiPlayer);
            return go;
        }



        private int minimax(string player)
        {
            List<int> availSpots = emptyRoom();
            if (checkWin(Player.huPlayer))
            { return -10;
            }
            else if (checkWin(Player.aiPlayer))
            {
                return 10;
            }
            else if (availSpots == null) return 0;


            List<Move> moves = new List<Move>();
            
            for (int i = 0; i < availSpots.Count; i++)
            {
                Move move = new Move();
                move.index = Int32.Parse(matrix[availSpots[i]].Tag.ToString());
                matrix[availSpots[i]].Content = player;
                if (player == Player.aiPlayer)
                {
                     move.score = minimax(Player.huPlayer);                     
                }
                   else if (player == Player.huPlayer)
                {
                    move.score = minimax(Player.aiPlayer);
                }
                matrix[availSpots[i]].Content = "";
                moves.Add(move);

            }
            if (player == Player.aiPlayer)
            {
                var bestScore = -10000;
                for (var i = 0; i < moves.Count; i++)
                {
                    if (moves[i].score > bestScore)
                    {
                        bestScore = moves[i].score;
                        bestMove = i;
                    }
                }
            }else
            {
                var bestScore = 10000;
                for (var i = 0; i < moves.Count; i++)
                {
                    if (moves[i].score < bestScore)
                    {
                        bestScore = moves[i].score;
                        bestMove = i;
                    }
                }
            }
            go = moves[bestMove].index;
            return moves[bestMove].score;

        }





        private List<int> emptyRoom()
        {
            List<int> emt = new List<int>();
            bool check = false;
            for (int i = 0; i < 9; i++)
            {
                if (matrix[i].Content.ToString()=="")
                {
                    emt.Add(i);
                    check = true;
                }                  
            }


            if (!check) return null;
            return emt;
        }

        private bool checkTie()
        {
            if (emptyRoom() != null)
            {
                return false;
            }
            else
            {
                MessageBox.Show("hoa nhau");
                return true;
            }

        }

        private void turn(int id, string player)
        {
            matrix[id].Content = player;
            AddToBoard();
            var gameWon = checkWin(player);
            if (gameWon)
            {
                announce(player);
                isOver = true;
            }
        }

        private void announce(string player)
        {
            for (int i = 0; i < 9; i++)
            {
                if (i == Player.winCombo[index][0] || i == Player.winCombo[index][1] || i == Player.winCombo[index][2]) {
                    matrix[i].Background = Brushes.Aqua;
                    continue;
                }
                matrix[i].IsEnabled = false;

            }
            AddToBoard();

            MessageBox.Show("winer is"+player);
        }

        private bool checkWin(string player)
        {
            for (int i = 0; i < Player.winCombo.Count; i++)
            {
                var chiso1 = Player.winCombo[i][0];
                var chiso2 = Player.winCombo[i][1];
                var chiso3 = Player.winCombo[i][2];

                if (matrix[chiso1].Content.ToString() == player && matrix[chiso2].Content.ToString() == player && matrix[chiso3].Content.ToString() == player)
                {
                    index = i;
                    return true;
                }
            }
            return false;
        }

        private void Replay_Click(object sender, RoutedEventArgs e)
        {
            StartGame();
        }
    }

    public class Move
    {
       public int index;
        public int score;
    }
}
